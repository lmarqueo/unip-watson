package br.com.unip.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unip.service.ConversationService;
import br.com.unip.watson.request.ConversationRequest;
import br.com.unip.watson.request.ConversationResponse;

@RestController
@RequestMapping("/conversation")
public class ConversationController {

	@Autowired
	private ConversationService conversationService;
	
	@PostMapping
	public ResponseEntity<ConversationResponse> conversation(@RequestBody ConversationRequest conversationRequest) {
		
		ConversationResponse conversationResponse = this.conversationService.conversation(conversationRequest);
	
		return ResponseEntity.ok(conversationResponse);
	}
	
}
