package br.com.unip.service;

import java.time.LocalDateTime;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import br.com.unip.domain.Input;
import br.com.unip.dto.MessageDTO;
import br.com.unip.feign.WatsonFeign;
import br.com.unip.feign.dto.WatsonRequest;
import br.com.unip.feign.dto.WatsonResponse;
import br.com.unip.watson.request.ConversationRequest;
import br.com.unip.watson.request.ConversationResponse;

@Service
public class ConversationService {

    private static final Logger LOG = LoggerFactory.getLogger(ConversationService.class);

    @Autowired
    private Environment env;

    @Autowired
    private WatsonFeign watsonFeign;
    
	public ConversationResponse conversation(ConversationRequest conversationRequest) {

		return this.callNlp(conversationRequest);
	}
	
    private ConversationResponse callNlp(ConversationRequest conversationRequest) {
    	   	    	
        WatsonRequest request = this.prepareRequest(conversationRequest);
        LOG.info("Request: " + conversationRequest);

        WatsonResponse response = this.watsonFeign.callNlp(request, getToken());
        
        ConversationResponse conversationResponse = this.prepareRequestFacebook(conversationRequest,  response);
        LOG.info("Response: " + conversationResponse);
        
        return conversationResponse;
    }

   	private String getToken() {
    	
        String credentials = "apikey:" + this.env.getProperty("watson.apikey");
        byte[] plainCredsBytes = credentials.getBytes();
        
        return "Basic " + DatatypeConverter.printBase64Binary(plainCredsBytes);
    }

    private WatsonRequest prepareRequest(ConversationRequest conversationRequest) {
    	
        return WatsonRequest.builder()
                            .input(new Input(conversationRequest.getMessage().getContent()))
                            .context(conversationRequest.getContext())
                            .build();
    }

    private ConversationResponse prepareRequestFacebook(ConversationRequest conversationRequest, WatsonResponse watsonResponse) {
    	
    	MessageDTO message = MessageDTO.builder()
					    		 	   .user(conversationRequest.getMessage().getUser())
					    		 	   .content(watsonResponse.getOutput().getGeneric().get(0).getText())
					    		 	   .date(LocalDateTime.now())
					    		 	   .build();
    	
        return ConversationResponse.builder()
                                   .message(message)
                                   .context(watsonResponse.getContext())
                                   .build();
    }

}
