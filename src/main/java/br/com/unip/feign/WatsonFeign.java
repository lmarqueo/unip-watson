package br.com.unip.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.unip.feign.dto.WatsonRequest;
import br.com.unip.feign.dto.WatsonResponse;

@FeignClient(name = "watson-nlp", url = "https://api.us-south.assistant.watson.cloud.ibm.com/instances/a3f63849-39e4-4ae6-ae70-827353ba18ba/v1/workspaces/539bd66c-5d85-4bf5-a077-fa1ebfbdf5a5/message?version=2020-04-01")
public interface WatsonFeign {

    @PostMapping
    public WatsonResponse callNlp(@RequestBody WatsonRequest watsonRequest, @RequestHeader(name = "Authorization") String token );
    
}
