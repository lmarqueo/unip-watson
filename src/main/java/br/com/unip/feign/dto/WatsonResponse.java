package br.com.unip.feign.dto;

import java.io.Serializable;
import java.util.List;

import br.com.unip.domain.Entity;
import br.com.unip.domain.Input;
import br.com.unip.domain.Intent;
import br.com.unip.domain.Output;
import br.com.unip.watson.Context;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Builder
@ToString
public class WatsonResponse implements Serializable {
	private static final long serialVersionUID = 2080403724982582166L;

	private List<Intent> intents;
	
	private List<Entity> entities;

    private Input input;

    private Output output;
    
    private Context context;

}
