package br.com.unip.feign.dto;

import java.io.Serializable;

import br.com.unip.domain.Input;
import br.com.unip.watson.Context;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Builder
@ToString
public class WatsonRequest implements Serializable {
	private static final long serialVersionUID = -168183334688015652L;

	private Input input;
    
    private Context context;
    
}
