package br.com.unip.domain;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class Output implements Serializable {
	private static final long serialVersionUID = 7299337861655381477L;
	
	private List<Generic> generic;

}
