package br.com.unip.domain;

import java.io.Serializable;
import java.time.LocalTime;

import br.com.unip.watson.request.ConversationRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class Generic implements Serializable {
	private static final long serialVersionUID = -3359601899020767864L;

	private String response_type;

	@Setter
    private String text;

	public void replaceText(ConversationRequest conversationRequest) {
		
		if(this.text.contains("[user]")) {
		
			this.text = this.text.replace("[user]", 
					conversationRequest.getMessage().getUser().getFirst_name() + " " + conversationRequest.getMessage().getUser().getLast_name());
			
			LocalTime now = LocalTime.now();
			
			String saudacao = "";
			if(now.isAfter(LocalTime.of(12, 00))) {
				
				if(now.isAfter(LocalTime.of(18, 00))) {
					
					saudacao = "boa noite";
				} else {
					
					saudacao = "boa tarde";
				}
			} else {
				
				saudacao = "bom dia";
			}
			
			this.text = this.text.replace("[periodo_do_dia]", saudacao);
		}
	} 

}
