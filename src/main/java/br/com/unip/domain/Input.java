package br.com.unip.domain;

import java.io.Serializable;

import org.springframework.util.StringUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@Getter
@ToString
public class Input implements Serializable {
	private static final long serialVersionUID = -2521207767297514801L;
	
	private String text;

	public Input(String text) {

		this.text = (StringUtils.isEmpty(text)) ? " " : text;
	}

}
