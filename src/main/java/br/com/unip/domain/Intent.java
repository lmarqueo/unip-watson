package br.com.unip.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
@ToString
public class Intent implements Serializable {
	private static final long serialVersionUID = -7877457767828890415L;

	private String intent;

    private Double confidence;

}
