package br.com.unip.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Metadata implements Serializable {
	private static final long serialVersionUID = 6765454798213224864L;

	private String deployment;

    private String user_id;

}
