package br.com.unip.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
@ToString
public class Entity implements Serializable {
	private static final long serialVersionUID = 2647909402053094991L;

	private String entity;
	
	private String value;
	
}
